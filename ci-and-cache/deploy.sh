#!/usr/bin/env bash

set -eux

config="$1"
remote="$2"

profile=/nix/var/nix/profiles/system

arch="$(ssh "$remote" uname -m)"

if [ "$arch" == "$(uname -m)" ]; then
    builders=""
else
    builders="ssh://$remote $arch-linux"
fi

here="$(dirname "$0")"
toplevel=$(nix-build "$here" -A arch.$arch.$config.toplevel --builders "$builders")

nix-copy-closure --use-substitutes --gzip --to "$remote" "$toplevel"
ssh "$remote" nix-env --profile "$profile" --set "$toplevel"
ssh "$remote" "$profile/bin/switch-to-configuration" switch
