{ lib, config, pkgs, modulesPath, ... }:

{
  imports = [
    "${modulesPath}/virtualisation/amazon-image.nix"

    # NOTE this profile is known to cause instability
    # "${modulesPath}/profiles/hardened.nix"
  ];

  ec2.hvm = true;

  nix.extraOptions = ''
    keep-outputs = true
    sandbox-fallback = false # probably unecessary
  '';

  # for debugging CI by building manually
  users.extraUsers.x = {
    uid = 1000;
    isNormalUser = true;
  };

  # for debugging CI by building manually
  environment.systemPackages = with pkgs; [
    vim git gnumake
  ];

  # speed up build
  documentation.nixos.enable = false;
}
